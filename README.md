# esp32web-multiuser-control

Relé control Sketch supporting concurrent shared controls via Web UI.

WiFi connectivity is signalled via a dedicated GPIO pin / LED.


## Arduino IDE prerequisites

Requires the Arduino IDE (Arduino IDE v2 was used).
Tested on an ESP32 WROOM Module (works with "ESP32 Dev Module" in the Arduino IDE)

Required libraries, installable directly via the Arduino IDE "Manage Libraries":
* **esp32** by Espressif Systems: https://github.com/espressif/arduino-esp32
* **ESPAsyncWebSrv**: https://github.com/dvarrel/ESPAsyncWebSrv


You will have to insert your WiFi credentials in the header file `wifi_credentials.h`.



## Relays module

Used Relay module, compatible with 3.3V GPIO outputs:

4-channel relay module 5V with optocoupler low-level trigger compatible with Arduino and Raspberry Pi
* https://www.az-delivery.de/en/products/4-relais-modul


(other supposed "low-input" relay modules did not work.)


The relay module can be fed directly from the 5V pin of the ESP32. In this case,
leave the "external power supply" pin of the module **connected**.


In the case of more relays, you might consider to have an external 5V PSU with sufficient power
to supply the relays (to support the case in which they all are enabled).



# Credits

Based on:

- Rui Santos, ESP32 Relay Module – Control AC Appliances (Web Server) https://RandomNerdTutorials.com/esp32-relay-module-ac-web-server/
- Random Nerd Tutorials, ESP32 Web Server using Server-Sent Events (Update Sensor Readings Automatically): https://randomnerdtutorials.com/esp32-web-server-sent-events-sse/
- UUID function in Vanilla JS: https://dirask.com/posts/JavaScript-UUID-function-in-Vanilla-JS-1X9kgD (for unique browser session UUID generation)

